"""myapp URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf import settings
from django.conf.urls import url, include
from django.conf.urls.static import static
import portal.views as portal
import post_creation.views as post
import library.views as library
import authentication.views as authentication
import courses.views as courses


urlpatterns = [
    path('administration/', admin.site.urls),
    path("", portal.index, name="home"),
    path("admin/pr/", include("portal.urls")),
    path("user/", include("user_profile.urls")),
    path("post/<str:post_id>/<str:title>", post.posts, name="post page"),
    path("admin/p/", include("post_creation.urls")),
    path("admin/c/", include("courses.urls")),
    path("library", library.library, name="library"),
    path("authenticate", authentication.login_user, name="authentication"),
    path("admin/user/add", authentication.create_user, name="register"),
    path("logout", authentication.logout_user, name="logout"),

    path("courses", portal.courses, name="courses"),
    path("attendance", portal.attendance, name="attendance"),
    path("notification", portal.notification, name="notification"),
    path("records", portal.records, name="records"),
    path("result", portal.result, name="result"),
    path("start", portal.get_started, name="start"),
    path("table", portal.timetable, name="table"),
    path("voucher", portal.voucher, name="voucher"),
    path("<str:url>", portal.page404, name="error404"),
    path("allocated-courses", portal.lecturer_courses, name="allocated courses"),
    path("scoresheet", portal.lecturer_scoresheet, name="scoresheet"),
    path("scoresheet/upload", portal.lecturer_upload, name="upload"),
]+static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
