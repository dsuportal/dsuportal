import datetime
from django.contrib.auth.models import User
from user_profile.models import UserProfile
from django.core.exceptions import ObjectDoesNotExist

def global_variables(request):
    now = datetime.datetime.now()
    current_year = now.year
    short_name = ''
    user_profile = None
    if request.user.is_authenticated:
        short_name = request.user.get_short_name()
        user = User.objects.get(id=request.user.id)
        try: 
            user_profile = UserProfile.objects.get(user=user)
        except:
            user_profile = None
    

    return {
        'current_year': current_year,
        "short_name": short_name,
        'user_profile': user_profile
    }