from django.shortcuts import render, redirect
from .models import Course
from django.contrib.auth.decorators import login_required
from myapp.decorators import user_group
from django.contrib.auth.models import User

# Create your views here.
@login_required(login_url='authentication')
@user_group(allowed_roles=['admin'])
def all_courses(request):
    courses = Course.objects.all()
    context = {
        'courses': courses
    }
    return render(request, "admin_panel/courses.html", context)

@login_required(login_url='authentication')
@user_group(allowed_roles=['admin'])
def add_course(request):
    global add_course_msg
    add_course_msg = ''
    if request.method == "POST":
        code = request.POST.get("code")
        title = request.POST.get("title")
        unit = request.POST.get("unit")
        group = None
        if request.POST.get("group") == "core":
            group = "CORE"
        elif request.POST.get("group") == "elect":
            group = "ELECTIVE"
        elif request.POST.get("group") == "gns":
            group = "GENERAL STUDIES"
        Course.objects.create(course_code=code, course_title=title, course_unit=unit, grouping=group)
        add_course_msg = 'Course Added!'

    context = {
        'success': add_course_msg
    }
    return render(request, "admin_panel/add_course.html", context)

@login_required(login_url='authentication')
@user_group(allowed_roles=['admin'])
def remove_course(request, course_id):
    course = Course.objects.get(id=course_id)
    context = {
        'course': course
    }
    return render(request, "admin_panel/remove_course.html", context)

@login_required(login_url='authentication')
@user_group(allowed_roles=['admin'])
def confirm_remove(request, course_id):
    Course.objects.filter(id=course_id).delete()
    return redirect('all courses')

@login_required(login_url='authentication')
@user_group(allowed_roles=['admin'])
def allocate_course(request):
    staff = User.objects.filter(groups__name='staff')
    courses = Course.objects.all()
    context = {
        'lecturers': staff,
        'courses': courses
    }
    return render(request, "admin_panel/course-allocate.html", context)
    
@login_required(login_url='authentication')
@user_group(allowed_roles=['admin'])
def allocated_list(request):
    staff = User.objects.filter(groups__name='staff')
    courses = ""
    for i in range(5):
        courses += f"Course{i+1}, "
    context = {
        'lecturers': staff,
        'n': range(5),
        'c': courses[:-2]
    }
    return render(request, "admin_panel/allocated-list.html", context)