from django.db import models

# Create your models here.
GROUPING = (
    ("CORE", "Core"),
    ("ELECTIVE", "Elective"),
    ("GENERAL STUDIES", "General Studies")
)

class Course(models.Model):
    course_code = models.CharField(max_length=150, null=True, blank=True)
    course_title = models.CharField(max_length=150, null=True, blank=True)
    course_unit = models.CharField(max_length=150, null=True, blank=True)
    grouping = models.CharField(max_length=100, choices=GROUPING, default=None)
