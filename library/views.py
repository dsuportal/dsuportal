from django.shortcuts import render
from .models import Book
from django.contrib.auth.decorators import login_required

# Create your views here.
@login_required(login_url='authentication')
def library(request):
    queryset = Book.objects.all()
    context = {
        'objects': reversed(queryset)
    }
    return render(request, "library.html", context)